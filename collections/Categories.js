Categories = new Mongo.Collection('categories');


import SimpleSchema from 'simpl-schema';

SimpleSchema.extendOptions(['autoform']);
SimpleSchema.extendOptions(['denyUpdate']);
Categories.attachSchema(new SimpleSchema({

    nom: {
        type: String,
        label: "nom"
    },
    horaire: {
        type: String,
        label: "horaire"


    },
    adresse: {
        type: String,
        label: "adresse"

    },
    telephone: {
        type: String,
        label: "telephone"
    },
    description:
        {
            type: String,
            label: "description"

        },
    longitude: {
        type: String,
        label: "longitude"

    },
    latitude: {
        type: String,
        label: "latitude"

    },


}));