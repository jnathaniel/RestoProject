Article = new Mongo.Collection("Article");

import SimpleSchema from 'simpl-schema';


SimpleSchema.extendOptions(['autoform']);
SimpleSchema.extendOptions(['denyUpdate']);

// Création du schéma des articles


Article.attachSchema(new SimpleSchema({

    title: {

        type: String,

        label: "Titre",

        max: 200

    },


    author: {
        type: String,
        label: "author",

    },
    produit: {
        type: Array,
        optional: false,
        label: "produit"
    },
    "produit.$": {
        type: Object
    },
    "produit.$.nom": {
        type: String,
        label: "NomProdut"
    },
}));