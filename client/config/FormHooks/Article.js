AutoForm.hooks({
    'insertArticle': {
        onSubmit: function (doc) { // Gestion du formulaire d'inscription
            console.log(doc);
            let error = null;
            let title = doc.title;
            let author = doc.author;
            let produit = doc.produit[0].nom;
            alert("wow");

            Article.insert({
                title: title,
                produit: produit,
                author: author,



            }, function (err) {
                if (err) {
                    error = new Error("Une erreur s'est produite");
                }
            });

            if (error === null) {
                this.done();
            }
            else {
                this.done(error); // Appelle onError
            }
            return false;
        },

        onSuccess: function () {
            Router.go('home');
        },

        onError: function (formType, err) {
            console.log(err)
        }
    },
});