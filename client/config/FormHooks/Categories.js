AutoForm.hooks({
    'insertCategories': {
        onSubmit: function (doc) { // Gestion du formulaire d'inscription
            console.log(doc);
            let error = null;
            let nom = doc.nom;
            let telephone = doc.telephone;
            let horaire = doc.horaire;
            let adresse = doc.adresse;
            let description = doc.description;
            let latitude = doc.latitude;
            let longitude = doc.longitude;


            Categories.insert({
                nom: nom,
                telephone: telephone,
                horaire: horaire,
                adresse: adresse,
                description: description,
                latitude: latitude,
                longitude: longitude,

            }, function (err) {
                if (err) {
                    error = new Error("Une erreur s'est produite");
                }
            });

            if (error === null) {
                this.done();
            }
            else {
                this.done(error); // Appelle onError
            }
            return false;
        },

        onSuccess: function () {
            Router.go('home');
        },

        onError: function (formType, err) {
            console.log(err)
        }
    },
});