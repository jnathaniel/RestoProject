UI.registerHelper('getGlobal', function(varName) {
    return Globals[varName];
});

UI.registerHelper('setTitle', function(title){
    if(!title){
        title = Globals.appName;
    }
    else{
        title += " - " + Globals.appName;
    }

    document.title = title;
});

UI.registerHelper('getCategories', function () {
        let datas = [];
        Categories.find({}).forEach(function (myDoc) {
            datas.push(myDoc)
        });
        console.log(datas);
        return datas;

    });

UI.registerHelper('getArticle', function () {
    let datas = [];
    Article.find({}).forEach(function (myDoc) {
        datas.push(myDoc)
    });
    console.log(datas);
    return datas;

});