Router.configure({
    layoutTemplate: "mainLayout"
});

Router.route('/', {
    name: "home",
    template: "home",


});

Router.route('/register', {
    name: "user.register",
    template: "register"
});

Router.route('/login', {
    name: "user.login",
    template: "login",

});


Router.route('/categories',
    {
        name: "Categories.Categories",
        template: "addCategories",
    },


    Router.route('/article/list',
        {
            name: "article.list",
            template: "articlelist"
        }
    ),
    Router.route('/article/show',
        {
            name: "article.show",
            template: "articleshow"
        }
    ),
    Router.route('/addArticle',
        {
            name: "addArticle",
            template: "addArticle"
        }
    ),
    Router.route('/article/edit',
        {
            name: "article.edit",
            template: "articleedit"
        }
    ),
    Router.route("/map",
        {

            name: "map",
            template: "map"
        },
    )
);